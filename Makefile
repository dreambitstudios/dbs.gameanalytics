#!/usr/bin/make

#
# envirment setup
#

# virtualenv related stuff
VIRTUALENV_PROMPT = dbs.gameanalytics

# python executables
PYTHON := $(shell if [ ! -z "`python --version 2>&1 | grep 'Python 2'`" ] ; then echo python; else echo python2; fi)
VIRTUAL_PYTHON = virtual/bin/python
PIP = virtual/bin/pip

#
# default
#

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo ""
	@echo "  <empty>                create development enviroment"
	@echo "  clean                  deletes files created by using/installing application"
	@echo "  clean-pyc              deletes only *.pyc files"
	@echo "  build-test-egg         generates installable egg with git short hash added to version number"

#
# Helpers
#

clean: clean-pyc
	rm -rf virtual
	rm -rf develop-eggs
	rm -rf build
	rm -rf dist
	rm tags
	rm .installed.cfg
	find . -name "*.egg-info" -exec rm -rf '{}' \;

clean-pyc:
	find . -name "*.pyc" -exec rm -rf '{}' \;

#
# Testing enviroment
#

build-test-egg:
	$(VIRTUAL_PYTHON) setup.py egg_info \
		-b`git log --pretty=format:'_%h' -n 1`_DEV \
		sdist rotate -m.tar.gz -k1

build-egg:
	$(VIRTUAL_PYTHON) setup.py sdist

upload:
	python setup.py sdist upload -r pypi
